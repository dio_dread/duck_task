/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.atsvil.labyrinthwalker.core;

import com.atsvil.labyrinthwalker.entity.Direction;
import com.atsvil.labyrinthwalker.state.MovementController;
import com.atsvil.labyrinthwalker.userinterface.InputQueue;
import com.atsvil.labyrinthwalker.userinterface.UserFrame;

/**
 *
 * @author Ales
 */
public class UserController extends AbstractPlayer {

    private MovementController walkerCtrl;
    private InputQueue inputQueue;

    public UserController(MovementController walkerCtrl, InputQueue inputQueue) {
        this.walkerCtrl = walkerCtrl;
        this.inputQueue = inputQueue;
    }

    @Override
    public MovementController getWalkerCtrl() {
        return this.walkerCtrl;
    }

    @Override
    public void playerMove() {
        Direction direction = this.inputQueue.nextInput();
        if (direction != null) {
            this.walkerCtrl.moveWalker(direction);
        }
    }

}
