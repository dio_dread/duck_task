/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.atsvil.labyrinthwalker.core;

import com.atsvil.labyrinthwalker.entity.Duck;
import com.atsvil.labyrinthwalker.entity.Labyrinth;
import com.atsvil.labyrinthwalker.entity.Movable;
import com.atsvil.labyrinthwalker.entity.Position;
import com.atsvil.labyrinthwalker.state.LabyrinthKeeper;
import com.atsvil.labyrinthwalker.state.MovementController;
import com.atsvil.labyrinthwalker.userinterface.UserFrame;
import com.atsvil.labyrinthwalker.userinterface.LabyrinthRender;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Ales
 */
public class Runner {

    private Configurator config;
    private UserFrame userFrame;
    private LabyrinthRender render;
    private Position previousPosition;
    private Movable[] walkers;
    private MovementController[] walkerCtrls;
    private AbstractPlayer[] players;
    private UserController userPlayer;
    private ArtificialWalker[] cpuPlayers;
    private LabyrinthKeeper game;

    public Runner(Configurator config) {

        Labyrinth labyrinth = null;
        try {
            labyrinth = new Labyrinth(
                    config.getLabyrinth(),
                    walkers,
                    config.getWallSign(),
                    config.getPlayerSign(),
                    config.getWayOutSign()
            );
        } catch (IOException ex) {
            Logger.getLogger(Runner.class.getName()).log(Level.SEVERE, null, ex);
        }

        this.config = config;
        this.render = new LabyrinthRender(labyrinth);
        this.userFrame = new UserFrame("Labyrinth", render);
        this.previousPosition = null;
        this.walkers = new Movable[config.getAiCount() + 1];
        this.walkerCtrls = new MovementController[config.getAiCount() + 1];
        this.players = new AbstractPlayer[config.getAiCount() + 1];
        this.cpuPlayers = new ArtificialWalker[config.getAiCount()];

        for (int num = 0; num < config.getAiCount() + 1; num++) {

            this.walkers[num] = new Duck(setupPlayerPosition(), labyrinth);
            this.walkerCtrls[num] = new MovementController(this.walkers[num]);
            if (num == 0) {
                this.userPlayer = new UserController(this.walkerCtrls[num], this.userFrame.getInputQueue());
                this.players[num] = this.userPlayer;
            } else {
                this.players[num] = new ArtificialWalker(this.walkerCtrls[num]);
            }
        }

        this.game = new LabyrinthKeeper(labyrinth, this.players);
        this.runAsunchronousThreads();
        this.launchArtificialWalkers();
        this.game.startLabyrinthChallenge();
    }

    private Position setupPlayerPosition() {
        String[] coords = this.config.getStartPosition().split("\\|");
        String[] leftCorner = coords[0].split("\\:");
        String[] rightCorner = coords[1].split("\\:");
        Position leftTopCorner = new Position(Integer.parseInt(leftCorner[1]), Integer.parseInt(leftCorner[0]));
        Position rightBottomCorner = new Position(Integer.parseInt(rightCorner[1]), Integer.parseInt(rightCorner[0]));

        if (null == this.previousPosition) {
            this.previousPosition = leftTopCorner;
        }
        
        if (this.previousPosition.getxCoord() + 1 == rightBottomCorner.getxCoord()) {
            this.previousPosition.setyCoord(this.previousPosition.getyCoord() + 1);
        }
        this.previousPosition.setxCoord(this.previousPosition.getxCoord() + 2);

        return new Position(this.previousPosition);
    }

    private void runAsunchronousThreads() {
        new Thread(this.userFrame).start();
    }

    private void launchArtificialWalkers() {
        int ind = 0;
        for (AbstractPlayer player : this.players) {
            if (!(player instanceof UserController)) {
                new Thread((ArtificialWalker) player, "AI - " + ind++).start();
            }
        }
    }
}
