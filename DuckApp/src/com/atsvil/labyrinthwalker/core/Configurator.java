/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.atsvil.labyrinthwalker.core;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Ales
 */
public class Configurator {

    private final Properties props;

    public Configurator(String fileName) {
        props = new Properties();
        try {
            props.load(new FileInputStream(fileName));
        } catch (IOException ex) {
            Logger.getLogger(Configurator.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Properties getProps() {
        return this.props;
    }

    public String[] getLabyrinth() throws IOException {
        return this.readLabyrinth(Paths.get(this.props.getProperty("labyrinthFile"))).split(",");
    }

    public char getWallSign() {
        return this.props.getProperty("wallSign").charAt(0);
    }

    public int getAiCount() {
        return Integer.parseInt(this.props.getProperty("aiCount"));
    }

    public char getWayOutSign() {
        return this.props.getProperty("wayOutSign").charAt(0);
    }
    
    public char getPlayerSign() {
        return this.props.getProperty("playerSign").charAt(0);
    }
    
    public String getStartPosition() {
        return this.props.getProperty("startPosition");
    }

    private String readLabyrinth(Path path) throws IOException {
        return Files.readAllLines(path).toString();
    }
}
