/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.atsvil.labyrinthwalker.core;

import com.atsvil.labyrinthwalker.entity.Direction;
import com.atsvil.labyrinthwalker.state.MovementController;
import com.atsvil.labyrinthwalker.userinterface.InputQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Ales
 */
public class ArtificialWalker extends AbstractPlayer implements Runnable {

    private MovementController walkerCtrl;
    private Direction direction;
    private Direction previousWall;
    private InputQueue inputQueue;

    public ArtificialWalker(MovementController walkerCtrl) {
        this.walkerCtrl = walkerCtrl;
        this.direction = Direction.UP;
        this.previousWall = null;
        this.inputQueue = new InputQueue();

    }

    @Override
    public void playerMove() {
        Direction direction = this.inputQueue.nextInput();
        if (direction != null) {
            this.walkerCtrl.moveWalker(direction);
        }
    }

    @Override
    public void run() {
        while (true) {
            this.checkDirection();
            this.inputQueue.offerInput(this.direction);

            try {
                Thread.sleep(150);
            } catch (InterruptedException ex) {
                Logger.getLogger(ArtificialWalker.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public MovementController getWalkerCtrl() {
        return this.walkerCtrl;
    }

    private void checkDirection() {
        if (this.isCorner()) {
            this.checkCorners();
        } else if (this.walkerCtrl.getWalker().isWallonTheLeft()) {
            if (this.direction == Direction.DOWN) {
                this.direction = Direction.DOWN;
            } else {
                this.direction = Direction.UP;
            }
            this.previousWall = Direction.LEFT;
        } else if (this.walkerCtrl.getWalker().isWallAbove()) {
            if (this.direction == Direction.LEFT) {
                this.direction = Direction.LEFT;
            } else {
                this.direction = Direction.RIGHT;
            }
            this.previousWall = Direction.UP;
        } else if (this.walkerCtrl.getWalker().isWallonTheRight()) {
            if (this.direction == Direction.UP) {
                this.direction = Direction.UP;
            } else {
                this.direction = Direction.DOWN;
            }
            this.previousWall = Direction.RIGHT;
        } else if (this.walkerCtrl.getWalker().isWallBelow()) {
            if (this.direction == Direction.RIGHT) {
                this.direction = Direction.RIGHT;
            } else {
                this.direction = Direction.LEFT;
            }
            this.previousWall = Direction.DOWN;
        } else {
            this.checkWallEndings();
        }

    }

    private boolean isCorner() {
        return (this.walkerCtrl.getWalker().isWallonTheLeft()
                & this.walkerCtrl.getWalker().isWallAbove())
                || (this.walkerCtrl.getWalker().isWallAbove()
                & this.walkerCtrl.getWalker().isWallonTheRight())
                || (this.walkerCtrl.getWalker().isWallonTheRight()
                & this.walkerCtrl.getWalker().isWallBelow())
                || (this.walkerCtrl.getWalker().isWallonTheLeft()
                & this.walkerCtrl.getWalker().isWallBelow());
    }

    private void checkCorners() {

        if (this.walkerCtrl.getWalker().isWallonTheLeft()
                & this.walkerCtrl.getWalker().isWallAbove()) {
            if (this.direction == Direction.LEFT) {
                this.direction = Direction.DOWN;
                this.previousWall = Direction.LEFT;
            } else {
                this.direction = Direction.RIGHT;
                this.previousWall = Direction.UP;
            }
        } else if (this.walkerCtrl.getWalker().isWallAbove()
                & this.walkerCtrl.getWalker().isWallonTheRight()) {
            if (this.direction == Direction.RIGHT) {
                this.direction = Direction.DOWN;
                this.previousWall = Direction.RIGHT;
            } else {
                this.direction = Direction.LEFT;
                this.previousWall = Direction.RIGHT;
            }
        } else if (this.walkerCtrl.getWalker().isWallonTheRight()
                & this.walkerCtrl.getWalker().isWallBelow()) {
            if (this.direction == Direction.RIGHT) {
                this.direction = Direction.UP;
                this.previousWall = Direction.RIGHT;
            } else {
                this.direction = Direction.LEFT;
                this.previousWall = Direction.DOWN;
            }
        } else if (this.walkerCtrl.getWalker().isWallonTheLeft()
                & this.walkerCtrl.getWalker().isWallBelow()) {
            if (this.direction == Direction.DOWN) {
                this.direction = Direction.RIGHT;
                this.previousWall = Direction.DOWN;
            } else {
                this.direction = Direction.UP;
                this.previousWall = Direction.LEFT;
            }
        }

    }

    private void checkWallEndings() {
        if (!this.walkerCtrl.getWalker().isWallonTheLeft()
                && !this.walkerCtrl.getWalker().isWallAbove()
                && !this.walkerCtrl.getWalker().isWallonTheRight()
                && !this.walkerCtrl.getWalker().isWallBelow()) {
            if (null != this.previousWall) {
                switch (this.previousWall) {
                    case LEFT:
                        this.direction = Direction.LEFT;
                        break;
                    case RIGHT:
                        this.direction = Direction.RIGHT;
                        break;
                    case UP:
                        this.direction = Direction.UP;
                        break;
                    case DOWN:
                        this.direction = Direction.DOWN;
                        break;
                    default:
                        break;
                }
            }
        }
    }

}
