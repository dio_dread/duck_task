/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.atsvil.labyrinthwalker.core;

import com.atsvil.labyrinthwalker.state.MovementController;

/**
 *
 * @author Ales
 */
public abstract class AbstractPlayer {
    
    public abstract void playerMove();
    
    public abstract MovementController getWalkerCtrl();
    
}
