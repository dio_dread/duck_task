/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.atsvil.labyrinthwalker.userinterface;

import com.atsvil.labyrinthwalker.entity.Direction;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.*;
import javax.swing.*;

public class UserFrame extends JFrame
        implements KeyListener, Runnable {

    private JTextArea typingArea;
    private InputQueue inputQueue;
    private LabyrinthRender render;

    public UserFrame(String name, LabyrinthRender render) {
        super(name);
        this.inputQueue = new InputQueue();
        this.render = render;
    }

    @Override
    public void run() {
        startKeyboardListener();
        while (true) {
            typingArea.setText(this.render.renderLabyrinth());
            try{
                Thread.sleep(50);
            } catch (InterruptedException ie) {
                System.out.println(ie.getMessage());
            }
        }
    }
    
    public InputQueue getInputQueue() {
        return this.inputQueue;
    }

    public void startKeyboardListener() {
        try {
            UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
        } catch (UnsupportedLookAndFeelException ex) {
            ex.printStackTrace();
        } catch (IllegalAccessException ex) {
            ex.printStackTrace();
        } catch (InstantiationException ex) {
            ex.printStackTrace();
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        UIManager.put("swing.boldMetal", Boolean.FALSE);
        createAndShowGUI();
    }

    private void createAndShowGUI() {
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setAlwaysOnTop(true);
        this.addComponentsToPane();

        this.pack();
        this.setVisible(true);
    }

    private void addComponentsToPane() {

        typingArea = new JTextArea(30, 60);
        Font font = new Font("Consolas", Font.BOLD, 12);
        typingArea.setFont(font);
        typingArea.setForeground(Color.BLUE);
        typingArea.setEditable(false);
        typingArea.addKeyListener(this);
        typingArea.setFocusTraversalKeysEnabled(false);
        typingArea.setText(this.render.renderLabyrinth());

        getContentPane().add(typingArea, BorderLayout.PAGE_START);
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
    }

    @Override
    public void keyReleased(KeyEvent e) {
        displayInfo(e, "KEY RELEASED: ");
    }

    private void displayInfo(KeyEvent e, String keyStatus) {
        int id = e.getID();

        if (id == KeyEvent.KEY_RELEASED) {
            int keyCode = e.getKeyCode();
            System.out.println(keyCode + " " + e.getKeyChar());
            switch (keyCode) {
                case 87:
                case 38:
                    inputQueue.offerInput(Direction.UP);
                    break;
                case 83:
                case 40:
                    inputQueue.offerInput(Direction.DOWN);
                    break;
                case 65:
                case 37:
                    inputQueue.offerInput(Direction.LEFT);
                    break;
                case 68:
                case 39:
                    inputQueue.offerInput(Direction.RIGHT);
                    break;
            }
        }
    }

}
