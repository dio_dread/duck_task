/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.atsvil.labyrinthwalker.userinterface;

import com.atsvil.labyrinthwalker.entity.Labyrinth;

/**
 *
 * @author Ales
 */
public class LabyrinthRender {
    
    private Labyrinth labyrinth;
    
    public LabyrinthRender(Labyrinth labyrinth) {
        this.labyrinth = labyrinth;
    }
    
    public String renderLabyrinth () {
        char[][] field = this.labyrinth.getField();
        String renderedField = "";
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[i].length; j++) {
                renderedField += field[i][j];
            }
            renderedField += "\n";
        }
        return renderedField;
    }
}
