/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.atsvil.labyrinthwalker.userinterface;

import com.atsvil.labyrinthwalker.entity.Direction;
import java.util.LinkedList;
import java.util.Queue;

/**
 *
 * @author Ales_Tsvil
 */
public class InputQueue {

    private Queue<Direction> input;

    public InputQueue() {
        this.input = new LinkedList<>();
    }

    public Direction nextInput() {
        return this.input.poll();
    }
    
    public void offerInput(Direction direction) {
        this.input.offer(direction);
    }
}
