/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.atsvil.labyrinthwalker;

import com.atsvil.labyrinthwalker.core.Configurator;
import com.atsvil.labyrinthwalker.core.Runner;

/**
 *
 * @author Ales_Tsvil
 */
public class DuckApp {
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Runner runner = new Runner(new Configurator("config\\config.properties"));
    }
    
}
