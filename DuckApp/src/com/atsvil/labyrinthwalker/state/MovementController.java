/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.atsvil.labyrinthwalker.state;

import com.atsvil.labyrinthwalker.entity.Direction;
import com.atsvil.labyrinthwalker.entity.Movable;

/**
 *
 * @author Ales
 */
public class MovementController {

    private final Movable walker;

    public MovementController(Movable walker) {
        this.walker = walker;
    }

    public void moveWalker(Direction direction) {
        switch (direction) {
            case UP:
                this.walker.moveUp();
                break;
            case DOWN:
                this.walker.moveDown();
                break;
            case LEFT:
                this.walker.moveLeft();
                break;
            case RIGHT:
                this.walker.moveRight();
                break;
        }
    }

    public Movable getWalker() {
        return this.walker;
    }

}
