package com.atsvil.labyrinthwalker.state;

import com.atsvil.labyrinthwalker.core.AbstractPlayer;
import com.atsvil.labyrinthwalker.core.ArtificialWalker;
import com.atsvil.labyrinthwalker.core.UserController;
import com.atsvil.labyrinthwalker.entity.Labyrinth;
import com.atsvil.labyrinthwalker.entity.Position;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;

/**
 *
 */
public class LabyrinthKeeper {

    private final Labyrinth labyrinth;
    private final AbstractPlayer[] players;
    private Queue<AbstractPlayer> playersQueue;
    private boolean isChallengeFinished = false;

    public LabyrinthKeeper(Labyrinth labyrinth, AbstractPlayer[] players) {
        this.labyrinth = labyrinth;
        this.players = players;
        this.createPlayersQueue();
    }

    public void startLabyrinthChallenge() {

        while (!isChallengeFinished) {
            AbstractPlayer player = this.playersQueue.poll();
            this.playersQueue.offer(player);
            player.playerMove();

            this.moveWalkers(player);
        }
    }

    private void finishLabyrinthChallenge() {
        isChallengeFinished = true;
        System.out.println("Challenge finished!");
    }

    private void createPlayersQueue() {
        this.playersQueue = new LinkedList<>();
        this.shufflePlayers();

        for (AbstractPlayer player : this.players) {
            this.playersQueue.offer(player);
        }
    }

    private void moveWalkers(AbstractPlayer player) {
        char walkerSign = '@';
        Position pos = null;
        Position prevPos = null;
        
            pos = player.getWalkerCtrl().getWalker().whereAmI();
            prevPos = player.getWalkerCtrl().getWalker().whereIWas();
        
        char[][] field = this.labyrinth.getField();

        if (this.labyrinth.isExitAt(pos.getxCoord(), pos.getyCoord())) {
            finishLabyrinthChallenge();
        }

        field[prevPos.getyCoord()][prevPos.getxCoord()] = ' ';
        field[pos.getyCoord()][pos.getxCoord()] = walkerSign;

    }

    private void shufflePlayers() {
        Random rand = new Random();

        for (int ind = 0; ind < this.players.length; ind++) {
            int randomInd = rand.nextInt(ind + 1);

            AbstractPlayer temp = this.players[randomInd];
            this.players[randomInd] = this.players[ind];
            this.players[ind] = temp;
        }
    }
}
