/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.atsvil.labyrinthwalker.entity;

/**
 * 
 * @author Ales
 */
public interface Positionable {
    public boolean isWallAbove();
    public boolean isWallBelow();
    public boolean isWallonTheLeft();
    public boolean isWallonTheRight();
    public Position whereIWas();
    public Position whereAmI();
    public Direction whereILookAt();
}
