/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.atsvil.labyrinthwalker.entity;

/**
 *
 * @author Ales
 */
public class Duck implements Movable {

    private final Labyrinth labirynth;
    private final Position position;
    private final Position previousPosition;
    private Direction direction;

    public Duck(Position pos, Labyrinth lab) {
        this.position = pos;
        this.previousPosition = new Position(pos.getxCoord() + 1, pos.getyCoord());
        this.labirynth = lab;
        this.direction = Direction.UP;
    }

    @Override
    public boolean isWallAbove() {
        return this.labirynth.isWallAt(this.position.getxCoord(), this.position.getyCoord() - 1);
    }

    @Override
    public boolean isWallBelow() {
        return this.labirynth.isWallAt(this.position.getxCoord(), this.position.getyCoord() + 1);
    }

    @Override
    public boolean isWallonTheLeft() {
        return this.labirynth.isWallAt(this.position.getxCoord() - 1, this.position.getyCoord());
    }

    @Override
    public boolean isWallonTheRight() {
        return this.labirynth.isWallAt(this.position.getxCoord() + 1, this.position.getyCoord());
    }

    @Override
    public Position whereAmI() {
        return this.position;
    }
    
    @Override
    public Position whereIWas() {
        return this.previousPosition;
    }

    @Override
    public Direction whereILookAt() {
        return this.direction;
    }

    @Override
    public void moveUp() {
        this.direction = Direction.UP;
        this.savePreviousPosition();
        if (!this.isWallAbove()) {
            this.position.setyCoord(this.position.getyCoord() - 1);
        }
    }

    @Override
    public void moveDown() {
        this.direction = Direction.DOWN;
        this.savePreviousPosition();
        if (!this.isWallBelow()) {
            this.position.setyCoord(this.position.getyCoord() + 1);
        }
    }

    @Override
    public void moveLeft() {
        this.direction = Direction.LEFT;
        this.savePreviousPosition();
        if (!this.isWallonTheLeft()) {
            this.position.setxCoord(this.position.getxCoord() - 1);
        }
    }

    @Override
    public void moveRight() {
        this.direction = Direction.RIGHT;
        this.savePreviousPosition();
        if (!this.isWallonTheRight()) {
            this.position.setxCoord(this.position.getxCoord() + 1);
        }
    }
    
    private void savePreviousPosition() {
        this.previousPosition.setxCoord(this.position.getxCoord());
        this.previousPosition.setyCoord(this.position.getyCoord());
    }

}
