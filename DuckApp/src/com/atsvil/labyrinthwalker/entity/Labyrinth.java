/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.atsvil.labyrinthwalker.entity;

/**
 *
 * @author Alesw
 */
public class Labyrinth {

    private char[][] field;
    private final char wallSign;
    private final char playerSign;
    private final char wayOutSign;

    public Labyrinth(String[] labirynthScheme, Movable[] walkers, char wallSign, char playerSign, char wayOutSign) {
        this.field = new char[labirynthScheme.length + 1][labirynthScheme[0].length() + 1];

        for (int line = 0; line < labirynthScheme.length; line++) {
            for (int sign = 0; sign < labirynthScheme[line].length(); sign++) {
                this.field[line][sign] = labirynthScheme[line].charAt(sign);
            }
        }
        this.wallSign = wallSign;
        this.playerSign = playerSign;
        this.wayOutSign = wayOutSign;

    }
    
    public char[][] getField() {
        return this.field;
    }
    
    public boolean isWallAt(int x, int y) {
        return this.field[y][x] == this.wallSign;
    }
    
    public boolean isExitAt(int x, int y) {
        return this.field[y][x] == this.wayOutSign;
    }
}
