/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.atsvil.labyrinthwalker.entity;

/**
 *
 * @author Ales
 */
public class Position {
    private int xCoord;
    private int yCoord;
    
    public Position() {
        this.xCoord = 0;
        this.yCoord = 0;
    }
    
    public Position(int x, int y) {
        this.xCoord = x;
        this.yCoord = y;
    }
    
    public Position(Position p) {
        this.xCoord = p.getxCoord();
        this.yCoord = p.getyCoord();
    }

    /**
     * @return the xCoord
     */
    public int getxCoord() {
        return xCoord;
    }

    /**
     * @param xCoord the xCoord to set
     */
    public void setxCoord(int xCoord) {
        this.xCoord = xCoord;
    }

    /**
     * @return the yCoord
     */
    public int getyCoord() {
        return yCoord;
    }

    /**
     * @param yCoord the yCoord to set
     */
    public void setyCoord(int yCoord) {
        this.yCoord = yCoord;
    }
}
